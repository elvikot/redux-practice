import "./index.css";

export default function TodoItem(props) {
  function changeStatus() {
    const updatedStatus = props.status === "undone" ? "done" : "undone";
    props.changeStatus(props.id, updatedStatus);
  }
  function removeTodo() {
    props.removeTodo(props.id);
  }

  return (
    <div className="todo-wrapper">
      <span
        className="todo-text"
        style={{
          textDecoration: props.status === "done" ? "line-through" : "none",
        }}
      >
        {props.text}
      </span>
      <button className="todo-status btn" onClick={changeStatus}>
        mark as {props.status === "done" ? "undone" : "done"}
      </button>
      <button className="todo-remove btn" onClick={removeTodo}>
        remove from list
      </button>
    </div>
  );
}
