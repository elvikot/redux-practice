import { useEffect, useState } from "react";
import TodoItem from "../todo-item";
import { useDispatch, useSelector } from "react-redux";
import {
  addTodo,
  getAllTodo,
  changeStatus,
  removeTodo,
} from "../../redux/actions/todo";
import "./index.css";

export default function TodoList() {
  const [isNewTodo, setNewTodo] = useState(false);
  const [inputValue, setInputValue] = useState("");
  const todos = useSelector((state) => state.todo.todo);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getAllTodo());
  }, [dispatch]);

  function addNewTodo() {
    if (!inputValue) return false;
    dispatch(addTodo(inputValue));
    let data = JSON.parse(localStorage.getItem("todo"));
    data
      ? data.push({ id: Date.now(), text: inputValue, status: "undone" })
      : (data = [{ id: Date.now(), text: inputValue, status: "undone" }]);
    localStorage.setItem("todo", JSON.stringify(data));
    setInputValue("");
    setNewTodo(false);
  }
  function removeSelectedTodo(id) {
    dispatch(removeTodo(id));
    let data = JSON.parse(localStorage.getItem("todo"));
    const todoIdToRemove = data.findIndex((item) => item.id === id);
    const newData = [
      ...data.slice(0, todoIdToRemove),
      ...data.slice(todoIdToRemove + 1),
    ];
    localStorage.setItem("todo", JSON.stringify(newData));
  }
  function changeTodoStatus(id, status) {
    dispatch(changeStatus(id, status));
    let data = JSON.parse(localStorage.getItem("todo"));
    const dataToUpdate = data.find((item) => item.id === id);
    console.log(dataToUpdate.status, status);
    dataToUpdate.status = status;
    console.log(dataToUpdate.status, status);
    const todoId = data.findIndex((item) => item.id === id);
    const newData = [
      ...data.slice(0, todoId),
      dataToUpdate,
      ...data.slice(todoId + 1),
    ];
    localStorage.setItem("todo", JSON.stringify(newData));
  }

  return (
    <div>
      <h2 className="title">My awesome todo list &#x2764;</h2>
      {todos.map((todo, id) => (
        <TodoItem
          key={id}
          id={todo.id}
          text={todo.text}
          status={todo.status}
          changeStatus={changeTodoStatus}
          removeTodo={removeSelectedTodo}
        />
      ))}
      {isNewTodo ? (
        <>
          <input
            type="text"
            className="input"
            onChange={(e) => {
              setInputValue(e.currentTarget.value);
            }}
            placeholder="insert your todo"
            value={inputValue}
          ></input>
          <button className="btn add-todo add-todo-btn" onClick={addNewTodo}>
            add
          </button>
        </>
      ) : (
        <button className="btn add-todo" onClick={() => setNewTodo(true)}>
          new todo
        </button>
      )}
    </div>
  );
}
