import logo from './logo.svg';
import './App.css';
import TodoList from "./components/todo-list";

function App() {
  return (
    <TodoList/>
  );
}

export default App;
