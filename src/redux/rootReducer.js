// Core
import { combineReducers } from "redux";
import { todoReducer as todo } from "./reducers/todoReducer";

// Reducers

export const rootReducer = combineReducers({
  todo,
});
