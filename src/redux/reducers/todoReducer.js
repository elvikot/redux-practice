import { todo } from "../types";

const initialState = {
  todo: [],
};

export function todoReducer(state = initialState, action) {
  switch (action.type) {
    case todo.GET_ALL_TODO:
      return { todo: [...state.todo, ...action.payload] };

    case todo.ADD_TODO:
      return { todo: [...state.todo, action.payload] };

    case todo.REMOVE_TODO:
      const todoIdToRemove = state.todo.findIndex(
        (item) => item.id === action.payload.id
      );
      return {
        todo: [
          ...state.todo.slice(0, todoIdToRemove),
          ...state.todo.slice(todoIdToRemove + 1),
        ],
      };

    case todo.CHANGE_STATUS:
      const dataToUpdate = state.todo.find(
        (item) => item.id === action.payload.id
      );
      const newTodo = { ...dataToUpdate, ...action.payload };
      const todoId = state.todo.findIndex(
        (item) => item.id === action.payload.id
      );
      return {
        todo: [
          ...state.todo.slice(0, todoId),
          newTodo,
          ...state.todo.slice(todoId + 1),
        ],
      };
    default:
      return state;
  }
}
