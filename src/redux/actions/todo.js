import { todo } from "../types";

export function getAllTodo() {
  const allTodo = JSON.parse(localStorage.getItem("todo"));
  return allTodo
    ? {
        type: todo.GET_ALL_TODO,
        payload: allTodo,
      }
    : {
        type: todo.GET_ALL_TODO,
        payload: [],
      };
}

export function addTodo(text) {
  return {
    id: Date.now(),
    type: todo.ADD_TODO,
    payload: { id: Date.now(), text: text, status: "undone" },
  };
}

export function removeTodo(id) {
  return {
    type: todo.REMOVE_TODO,
    payload: { id },
  };
}

export function changeStatus(id, status) {
  return {
    type: todo.CHANGE_STATUS,
    payload: { id, status },
  };
}
